import Point from "./point.js";
import Vector from "./vector.js";

export default class Thing {
  constructor(point, vector, strength) {
    this.position = point || new Point();
    this.vector = vector || new Vector();
    this.strength = strength || 100;
  }
}
