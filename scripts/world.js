import Thing from "./thing.js";
import Point from "./point.js";
import Vector from "./vector.js";
import Dice from "./dice.js";

export default class World {
  constructor(thingCount, thingSpeed, updateSpeed, size) {
    thingCount = thingCount || 25;
    thingSpeed = thingSpeed || 5;

    this.started = false;
    this.size = size || new Point(800, 600);
    this.updateSpeed = updateSpeed || 10;
    this.canvas = document.getElementById('bounce');
    this.things = [];

    const dice = new Dice();
    const getX = () => dice.random(this.size.x);
    const getY = () => dice.random(this.size.y);
    const getSpeed = () => dice.random(thingSpeed) * (dice.flip() ? -1 : 1);
    const getStrength = () => dice.between(20, 100);

    for(let i = 0; i < thingCount; i++) {
      this.things.push(new Thing(
        new Point(getX(), getY()),
        new Vector(getSpeed(), getSpeed()),
        getStrength()
      ));
    }
  }

  update() {
    for (let thing of this.things) {
      thing.position.x += thing.vector.x;
      thing.position.y += thing.vector.y;

      if (thing.position.x <= 0) {
        thing.position.x = 0;
        thing.vector.x *= -1;
      }
      else if (thing.position.x >= this.size.x) {
        thing.position.x = this.size.x;
        thing.vector.x *= -1;
      }

      if (thing.position.y <= 0) {
        thing.position.y = 0;
        thing.vector.y *= -1;
      }
      else if (thing.position.y >= this.size.y) {
        thing.position.y = this.size.y;
        thing.vector.y *= -1;
      }

    }
  }

  render() {
    const c = this.canvas.getContext('2d');
    const thingSize = 10;
    const thingOffset = thingSize * 0.5;

    c.fillStyle = '#000';
    c.fillRect(0, 0, this.canvas.width, this.canvas.height);

    for (let thing of this.things) {
      let a = thing.strength * 0.01;
      c.fillStyle = 'rgba(255, 255, 255, ' + a + ')';
      c.fillRect(thing.position.x - thingOffset, thing.position.y - thingOffset, thingSize, thingSize);
    }
  }

  activityLoop() {
    if (this.started) {
      this.update();
      setTimeout(this.activityLoop, this.updateSpeed);
    }
  }

  start() {
    if (!this.started) {
      console.log('starting');
      const self = this;

      this.started = true;

      (function updater() {
        if (self.started) {
          self.update();
          setTimeout(updater, 10);
        }
      })();

      (function animator() {
        if (self.started) {
          self.render();
          window.requestAnimationFrame(animator);
        }
      })();
    }
    return this;
  }

  stop() {
    console.log('stopping');
    this.started = false;
    return this;
  }
}
