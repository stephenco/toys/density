import Point from "./point.js";

export default class Vector extends Point {
  constructor(x, y, decay) {
    super(x, y);
    this.decay = decay;
  }
}
