export default class Dice {
  constructor() {
  }

  random(max) {
    return Math.random() * max;
  }

  between(min, max) {
    return this.random(max - min) + min;
  }

  roll(sides) {
    return Math.round(this.random(sides)) + 1;
  }

  flip() {
    return Math.round(Math.random());
  }
}
