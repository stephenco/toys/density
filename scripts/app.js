import World from "./world.js";

const world = new World(200).start();

window.onkeypress = function(e) {
  switch (e.charCode) {
    case 32: // space
      world.started ? world.stop() : world.start();
      break;
  }
};
